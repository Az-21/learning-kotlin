open class Mamal(name: String, fur: Boolean, hands: Int){

    var name: String? = null;
    var fur: Boolean? = null;
    var hands: Int? = null;

    init{
        this.name = name;
        this.fur = fur;
        this.hands = hands;
    }

}

class Dog(breed: String, color: String): Mamal("Canis lupus familiaris", true, 0){

    var breed: String? = null;
    var color: String? = null;

    init{
        this.breed = breed;
        this.color = color;
    }
}



fun main(args: Array<String>){

    var human = Mamal("Homo sapien", false, 2);

    var poodle = Dog("Poodle", "White");

    println(poodle.hands);

}