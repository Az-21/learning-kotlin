fun main(args: Array<String>){

    //Declaring variable
    var noOfApples = 5;
    var colorOfApple = "Green";



    //Print
    println(noOfApples);
    println(colorOfApple);
    println("You have ordered " + noOfApples + " " + colorOfApple + " apples.");



    //Reassigning variable
    colorOfApple = "Red";
    println(colorOfApple);
    /*Variable type must remain constant when reassigning.
     *colorOfApple = 123;       //ERROR
     *println(colorOfApple); */



    //Explicit variable assignment
    var myDouble: Double = 5.00000100001;
    println(myDouble);


    var myChar = "a";
    println(myChar);



    //User input
    println("Enter your name: ");
    var userName = readLine();

    println("Welcome $userName");
}