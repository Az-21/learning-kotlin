//Parent class
open class Mamal(){

    var name: String? = null;
    var fur: Boolean? = null;
    var hands: Int? = null;

    constructor(name: String, fur: Boolean, hands: Int): this(){
        this.name = name;
        this.fur = fur;
        this.hands = hands;
    }
}

//Child class
class Dog(){

    var breed: String? = null;
    var color: String? = null;
    var name: String = "Canis lupus familiaris";
    var fur: Boolean = true;
    var hands: Int = 0;

    constructor(breed: String, color: String): this(){
        this.breed = breed;
        this.color = color;
    }
}


fun main(args: Array<String>){
    var poodle = Dog("Poodle", "White");
    println(poodle.name);
    poodle.name = "Doggie";
    println(poodle.name);
}
